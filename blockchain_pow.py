import hashlib
import json
import time
import datetime
import requests
import threading
import jsonpickle
import psutil
import os
import sys

from urllib.parse import urlparse
from uuid import uuid4
from flask import Flask, jsonify, request, current_app
from collections import OrderedDict

########################
# Small help functions #
########################
def log(text):
	global logging
	if logging:
		print(text)
	return True

# Return the given bytes as a human friendly KB, MB, GB, or TB string
def convertbytes(B):
   B = float(B)
   KB = float(1024)
   MB = float(KB ** 2) # 1,048,576
   GB = float(KB ** 3) # 1,073,741,824
   TB = float(KB ** 4) # 1,099,511,627,776

   if B < KB:
      return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
   elif KB <= B < MB:
      return '{0:.2f} KB'.format(B/KB)
   elif MB <= B < GB:
      return '{0:.2f} MB'.format(B/MB)
   elif GB <= B < TB:
      return '{0:.2f} GB'.format(B/GB)
   elif TB <= B:
      return '{0:.2f} TB'.format(B/TB)	
	
def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
	
#####################################
# MerkleNode and MerkleTree classes #
#####################################

class MerkleNode():
	def __init__(self,data):
		self.data = data
		self.hash = hashlib.sha256(json.dumps(data, sort_keys=True).encode()).hexdigest()
		self.parent = None
		self.leftchild = None
		self.rightchild = None
	def update_parent(self,parent):
		self.parent = parent
	def update_children(self,left,right):
		self.leftchild = left
		self.rightchild = right

class MerkleTree():
	def __init__(self):
		self.root = ""
		self.nodes = []
		self.leaves = []
	def append_leaf(self, leaf):
		node = MerkleNode(leaf)
		self.leaves.append(node)
	def build_tree(self):
		self.nodes = []
		self.calc_tree(self.leaves)
	def calc_tree(self, nodes):
		self.nodes.extend(nodes)
		if len(nodes) == 1:
			self.root = nodes[0].hash
		else:
			parents = []
			for index in range(0,len(nodes),2):
				leftNode = nodes[index]
				if index+1 != len(nodes):
					rightNode = nodes[index+1]
				else:
					rightNode = leftNode
				parentdata = leftNode.hash+rightNode.hash
				parent = MerkleNode(parentdata)
				leftNode.update_parent(parent)
				if index+1 != len(nodes):
					rightNode.update_parent(parent)
					parent.update_children(leftNode,rightNode)
				else:
					parent.update_children(leftNode,None)
				parents.append(parent)
			self.calc_tree(parents)
	def audit_proof(self,leaf):
		if leaf in self.leaves:
			parent = leaf.parent
			trail = []
			return self.build_audit_trail(trail,parent,leaf)
	def build_audit_trail(self,trail,parent,child):
		if parent != None:
			if parent.leftchild == child:
				if parent.rightchild == None:
					trail.append(parent.leftchild)
				else:
					trail.append(parent.rightchild)
			else:
				trail.append(parent.leftchild)
			self.build_audit_trail(trail,parent.parent,parent)
		return trail
	def verify_audit_proof(self,root,leaf,trail):
		oldhash = leaf.hash
		for node in trail:
			if node.parent.rightchild == node:
				newhash = hashlib.sha256(oldhash.encode()+node.hash.encode()).hexdigest()
			else:
				newhash = hashlib.sha256(node.hash.encode()+oldhash.encode()).hexdigest()
			oldhash = newhash
		return root == newhash	
	
####################
# Blockchain class #
####################

class Blockchain:
	def __init__(self):
		self.current_transactions = []
		self.chain = []
		self.merkletree = MerkleTree()
		self.nodes = set()
		# Generate a globally unique address for this node
		self.node_identifier = str(uuid4()).replace('-', '')
		self.address = ""
		# Stats
		self.time_last_block = None
		self.time_last_consensus = None
		self.blocks_mined = 0
		self.average_mining_time = None		
		self.resolved_branches = 0
		# Create the genesis block
		self.new_block(previous_hash='1', proof=100)	
	def register_node(self, address):	
		parsed_url = urlparse(address)
		if parsed_url.netloc:
			self.nodes.add(parsed_url.netloc)
			# Once a new node is introduced to the network they need to resolve conflicts to sync their chains	
			self.resolve_conflicts([parsed_url.netloc])				
		elif parsed_url.path:
			# Accepts an URL without scheme like '192.168.0.5:5000'.
			self.nodes.add(parsed_url.path)
			# Once a new node is introduced to the network they need to resolve conflicts to sync their chains			
			self.resolve_conflicts([parsed_url.path])			
		else:
			raise ValueError('Invalid URL')			
	def valid_chain(self, chain):
		last_block = chain[0]
		current_index = 1
		while current_index < len(chain):
			block = chain[current_index]
			# Check that the hash of the block is correct
			last_block_hash = self.hash(last_block)
			if block['previous_hash'] != last_block_hash:
				return False
			# Check that the Proof of Work is correct
			if not self.valid_proof(last_block['proof'], block['proof'], last_block_hash):
				return False
			last_block = block
			current_index += 1
		return True	
	def resolve_conflicts(self, addresses=None):
		neighbours = self.nodes
		if addresses:
			neighbours = addresses
		new_chain = None
		# We are only looking for chains longer than ours
		max_length = len(self.chain)
		# Grab and verify the chains from all the nodes in our network
		for node in neighbours:
			try:
				response = requests.get(f'http://{node}/chain', timeout=10)
				if response.status_code == 200:
					length = response.json()['length']
					chain = response.json()['chain']
					merkletree = jsonpickle.decode( response.json()['merkletree'])		
				# Check if the length is longer and the chain is valid
				if length > max_length and self.valid_chain(chain):
					max_length = length
					new_chain = chain
			except requests.exceptions.RequestException as e:
				log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Connection Error")
				global connection_error_count
				connection_error_count = connection_error_count + 1				
		self.time_last_consensus = datetime.datetime.now()
		log(datetime.datetime.strftime(self.time_last_consensus, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Checking for consensus")					
		# Replace our chain if we discovered a new, valid chain longer than ours
		if new_chain and len(new_chain)>len(self.chain):
			log(datetime.datetime.strftime(self.time_last_consensus, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Replaced local chain with longer one")		
			self.chain = new_chain
			self.resolved_branches = self.resolved_branches+1
			self.merkletree = merkletree
			return True
		return False
	def send_new_block(self, block):
		values = {
			'block' :	block,
			'node_identifier' : self.node_identifier
		}	
		neighbours = self.nodes
		for node in neighbours:
			try:
				response = requests.post(f'http://{node}/block/new', json=values, timeout=10)	
				#threading.Thread(target=requests.post, args=(f'http://{node}/block/new',),kwargs={'json':values, 'timeout':10}).start()
			except requests.exceptions.RequestException as e:
				log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Connection Error")		
				global connection_error_count
				connection_error_count = connection_error_count + 1				
		return True	
	def new_block(self, proof, previous_hash):
		block = {
			'index': len(self.chain) + 1,
			'timestamp': time.time(),
			'transactions': self.current_transactions,
			'proof': proof,
			'previous_hash': previous_hash or self.hash(self.chain[-1]),
		}
		# Reset the current list of transactions
		self.current_transactions = []
		self.chain.append(block)
		self.merkletree.append_leaf(block)
		self.merkletree.build_tree()
		# Gathering statistics
		time_current_block = datetime.datetime.now()
		if len(self.chain)>1:
			self.blocks_mined = self.blocks_mined + 1
			if self.average_mining_time is None:
				self.average_mining_time = abs((starttime_mining - time_current_block).total_seconds())
			else:
				self.average_mining_time = ((self.average_mining_time*(self.blocks_mined-1))+abs((time_current_block - self.time_last_block).total_seconds()))/self.blocks_mined
			self.time_last_block = time_current_block
			log(datetime.datetime.strftime(self.time_last_block, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Created new block with ID "+str(block['index']))
			self.send_new_block(block)
		return block
	def new_transaction(self, usage, sender, recipient, amount):
		if usage!="Reward":
			# Before creating a new transaction you need to update your blockchain to the main chain
			self.resolve_conflicts()
		self.current_transactions.append({
			'usage': usage,
			'sender': sender,
			'recipient': recipient,
			'amount': amount,
		})
		if usage=="Registration":
			self.register_node(amount)		
		return self.last_block['index'] + 1
	def gather_data(self, sender):
		block = self.last_block
		n = 1
		result = []
		while block['index']!=1:
			for transaction in block['transactions']:
				if transaction['usage']=='Data':
					if sender!="all":
						if transaction['sender']==sender:
							result.append(transaction['amount'])
					else:
						result.append(transaction['amount'])
			n = n+1
			block = self.chain[-n]
		return result
	def last_transaction(self,sender):	
		# Before searching for the data you need to update your blockchain to the main chain
		self.resolve_conflicts()
		block = self.last_block
		n = 1
		result = None
		while block['index']!=1:
			for transaction in reversed(block['transactions']):
				if transaction['usage']=='Data':
					if transaction['sender']==sender:
						result = transaction['amount']
						return result
			n = n+1
			block = self.chain[-n]
		return result	
	@property
	def last_block(self):
		return self.chain[-1]
	@staticmethod
	def hash(block):
		# We must make sure that the Dictionary is Ordered, or we'll have inconsistent hashes
		block_string = json.dumps(block, sort_keys=True).encode()
		return hashlib.sha256(block_string).hexdigest()
	def proof_of_work(self, last_block):
		last_proof = last_block['proof']
		last_hash = self.hash(last_block)
		proof = 0
		while self.valid_proof(last_proof, proof, last_hash) is False:
			proof += 1
		return proof
	@staticmethod
	def valid_proof(last_proof, proof, last_hash):
		guess = f'{last_proof}{proof}{last_hash}'.encode()
		guess_hash = hashlib.sha256(guess).hexdigest()
		return guess_hash[:6] == "000000"

###############
# Miner class #
###############

# Thread for constantly mining in the background
class Miner(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.status = True
		self._is_running = True
	def run(self):
		global starttime_mining
		starttime_mining = datetime.datetime.now()
		log(datetime.datetime.strftime(starttime_mining, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : This node is mining now")
		while self._is_running:
			while self.status:
				mine()
		return True
	def stop(self):
		self.status = False
	def cont(self):
		self.status = True	
	def exit(self):
		self._is_running = False
		
# Starts a single mining run
def mine():
	# We run the proof of work algorithm to get the next proof...
	last_block = blockchain.last_block
	proof = blockchain.proof_of_work(last_block)
	# We must receive a reward for finding the proof.
	# The sender is "0" to signify that this node has mined a new coin.
	blockchain.new_transaction(
		usage="Reward",
		sender="0",
		recipient=blockchain.node_identifier,
		amount=1,
	)
	# Forge the new Block by adding it to the chain
	if last_block == blockchain.last_block:
		previous_hash = blockchain.hash(last_block)
		block = blockchain.new_block(proof, previous_hash)
	return True
	
################
# Sensor class #
################

# Thread for reading sensor data in the background
# Simulating a Sensor streaming Data
class Sensor(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.status = True
		self._is_running = True
	def run(self):
		starttime_sensor = datetime.datetime.now()
		log(datetime.datetime.strftime(starttime_sensor, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : This node is receiving sensordata now")	
		with open(filename) as f:
			initial = None
			while self._is_running:
				while self.status:
					content = f.readline()
					if content == "":
						log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : End of File recieved")	
						return True
					content = content.strip()
					neighbours = blockchain.nodes
					starttime_recieved = datetime.datetime.now()
					log(datetime.datetime.strftime(starttime_recieved, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Sensor recieved: "+content)					
					data = {
						'usage': "Data",
						'sender': blockchain.address,
						'recipient': "Test",
						'amount': content,
						'node_identifier': blockchain.node_identifier
					}
					# Create a new Transaction with the recieved Data
					# Broadcast the transaction to all the nodes in our network
					for node in neighbours:
						try:
							response = requests.post(f'http://{node}/transactions/new', json=data, timeout=10)
							#threading.Thread(target=requests.post, args=(f'http://{node}/transactions/new',),kwargs={'json':data, 'timeout':10}).start()							
						except requests.exceptions.RequestException as e:
							log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Connection Error")
							global connection_error_count
							connection_error_count = connection_error_count + 1
					# Add the transaction to your own list of transactions
					index = blockchain.new_transaction(data['usage'], data['sender'], data['recipient'], data['amount'])
					
					###################
					# Data processing #
					###################
					time.sleep(30)
					if initial == None:
						initial = float(content)
						timestamp = datetime.datetime.now()
						log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : The Initial value is "+str(initial))
						values = []
						values.append(float(content))
						for node in neighbours:
							while blockchain.last_transaction(node) == None:
								log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Waiting for Value!")
								time.sleep(10)
							values.append(float(blockchain.last_transaction(node)))
						# calculate reference
						sum = 0
						for value in values:
							sum += value
						reference = sum/len(values)
						timestamp = datetime.datetime.now()
						log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Recieved "+str(values)+" from the other nodes")						
						log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : The Reference is "+str(reference))						
					# calculate drift
					drift = float(content)-initial
					timestamp = datetime.datetime.now()
					log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : The Drift is "+str(drift))					
					# check_local_condition
					if not check_local_condition(reference,drift,5):
						# alert
						timestamp = datetime.datetime.now()
						log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Local Condition violated!")						
						# gather_data
						values = []
						values.append(float(content))
						for node in neighbours:
							values.append(float(blockchain.last_transaction(node)))
						# check_global_condition
						if not check_global_condition(values,5):
							# alert
							timestamp = datetime.datetime.now()
							log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Global Violation detected in "+
							str(abs((timestamp - starttime_recieved).total_seconds()))+" Seconds")
						else:
							# Change the reference
							reference = sum/len(values)
							timestamp = datetime.datetime.now()
							log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Recieved "+str(values)+" from the other nodes")						
							log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : The Reference is "+str(reference))							
					#######################
					# Data processing end #
					#######################
					time.sleep(10)
		return True
	def stop(self):
		self.status = False
	def cont(self):
		self.status = True
	def exit(self):
		self._is_running = False

#################################
# Convex/Concave bounds mehtods #
#################################

# Using a very easy function(Mean) to make testing easy 
def check_local_condition(reference,drift,threshhold):
	if reference+drift<=threshhold:
		return True
	else:
		return False

def check_global_condition(values,threshhold):
	sum = 0
	for value in values:
		sum += value
	mean = sum/len(values)
	if mean <= threshhold:
		return True
	else:
		return False
		
#####################
# Flask Application #
#####################

# Statistics
starttime = None
starttime_mining = datetime.datetime.now()
# Communication
request_chain_count = 0
broadcast_transaction_count = 0
block_recieved_count = 0
connection_error_count = 0
	
# Instantiate the Node
app = Flask(__name__)
# Instantiate the Blockchain
blockchain = Blockchain()	
# Filename for reading Sensor Data
filename = ""
		
# Instantiate Miner and Sensor
thread = Miner()
thread_running = False
sensor = Sensor()
sensor_running = False

#############################
# Flask adresses for mining #
#############################

# Start Mining
@app.route('/mine/start', methods=['GET'])
def mine_start(): 
	global thread_running
	if thread_running:
		thread.cont()
	else:
		thread.start()
		thread_running = True
	response = {
		'message' : "Mining initiated"
	}
	return jsonify(response), 200

# Stop Mining
@app.route('/mine/stop', methods=['GET'])
def mine_stop():
	thread.stop()
	response = {
		'message' : "Mining stopped"
	}
	return jsonify(response), 200

#################################
# Flask adresses for the sensor #
#################################

# Start the Sensor
@app.route('/sensor/start', methods=['GET'])
def sensor_start(): 
	global sensor_running
	if sensor_running:
		sensor.cont()
	else:
		sensor.start()
		sensor_running = True
	response = {
		'message' : "Sensor initiated"
	}
	return jsonify(response), 200

# Stop the Sensor
@app.route('/sensor/stop', methods=['GET'])
def sensor_stop():
	sensor.stop()
	response = {
		'message' : "Sensor stopped"
	}
	return jsonify(response), 200

###################################
# Flask adresses for transactions #
###################################	
	
# Add a transaction to all nodes
@app.route('/transactions/broadcast', methods=['POST'])
def broadcast_transaction():
	values = request.get_json(force=True)
	# Check that the required fields are in the POST'ed data
	required = ['usage', 'sender', 'recipient', 'amount'] 
	if not all(k in values for k in required):
		return 'Missing values', 400
	
	transactiontime = datetime.datetime.now()
	log(datetime.datetime.strftime(transactiontime, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Broadcating a new transaction: "+str(values))
	values['node_identifier'] = blockchain.node_identifier
	
	neighbours = blockchain.nodes
	# Broadcast the transaction to all the nodes in our network
	for node in neighbours:
		try:
			response = requests.post(f'http://{node}/transactions/new', json=values, timeout=10)
			#threading.Thread(target=requests.post, args=(f'http://{node}/transactions/new',),kwargs={'json':values, 'timeout':10}).start()	
		except requests.exceptions.RequestException as e:
			log(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Connection Error")	
			global connection_error_count
			connection_error_count = connection_error_count + 1
	# Add the transaction to your own list of transactions
	index = blockchain.new_transaction(values['usage'], values['sender'], values['recipient'], values['amount'])
	
	response = {'message': f'Transaction will be added to Block {index}'}
	return jsonify(response), 201

# Create a new transaction(for the current node), broadcast_transaction should be used
@app.route('/transactions/new', methods=['POST'])
def new_transaction():
	global broadcast_transaction_count
	broadcast_transaction_count = broadcast_transaction_count + 1
	values = request.get_json(force=True)
	# Check that the required fields are in the POST'ed data
	required = ['usage', 'sender', 'recipient', 'amount', 'node_identifier'] 
	if not all(k in values for k in required):
		return 'Missing values', 400
		
	node_identifier = values['node_identifier']
	del(values['node_identifier'])
	transactiontime = datetime.datetime.now()
	log(datetime.datetime.strftime(transactiontime, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Recieving a new transaction from "+node_identifier+": "+str(values))
		
	# Create a new Transaction	
	index = blockchain.new_transaction(values['usage'], values['sender'], values['recipient'], values['amount'])
	
	response = {'message': f'Transaction will be added to Block {index}'}
	return jsonify(response), 201
	
######################################
# Flask adresses for requesting data #
######################################	
	
# Return the whole chain
@app.route('/chain', methods=['GET'])
def full_chain():
	global request_chain_count
	request_chain_count = request_chain_count + 1
	response = {
		'chain': blockchain.chain,
		'length': len(blockchain.chain),
		'merkletree': jsonpickle.encode(blockchain.merkletree)
	}
	return jsonify(response), 200

# Return the unique identifier of the node
@app.route('/node/id', methods=['GET'])
def identify_node():
	response = {
		'node_identifier': blockchain.node_identifier
	}
	return jsonify(response), 200	

#################################################
# Flask adresses for configuring the blockchain #
#################################################	

# Track used ressources
@app.route('/stats', methods=['GET'])
def get_stats():
	global starttime
	global request_chain_count
	global broadcast_transaction_count
	global block_recieved_count
	global connection_error_count
	timestamp = datetime.datetime.now()
	process = psutil.Process(os.getpid())
	log("##############")
	log("# Statistics #")
	log("##############")
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" is running for "+
		str(abs((timestamp - starttime).total_seconds()))+" Seconds")	
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" has mined "+str(blockchain.blocks_mined)+
		" block(s) with an average mining time of: "+str(blockchain.average_mining_time)+" seconds")
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" has resolved "+
		str(blockchain.resolved_branches)+" Branches")		
	if len(blockchain.chain)>1:
		log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" : has an average time between blocks of: "+
			str(abs((timestamp - starttime).total_seconds())/len(blockchain.chain)))		
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : The blockchain of node "+blockchain.node_identifier+" is currently "+
		str(len(blockchain.chain))+" block(s) long: "+str(blockchain.chain))	
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" is using "+
		convertbytes(process.memory_info().rss)+" of memory")
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" is using "+
		str(process.cpu_percent(interval=1))+"% of the CPU")
	log(datetime.datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Communication: Request Chain: "
		+str(request_chain_count)+", Broadcast Transaction: "+str(broadcast_transaction_count)+", Block Recieved: "
		+str(block_recieved_count)+", Connection Error: "+str(connection_error_count))		
	log("##############")		
	response = {
		'message': 'These are the stats for this application'
	}
	return jsonify(response), 200
	
# Register the address of new nodes
@app.route('/nodes/register', methods=['POST'])
def register_nodes():
	values = request.get_json(force=True)
	nodes = values.get('nodes')
	if nodes is None:
		return "Error: Please supply a valid list of nodes", 400
	
	for node in nodes:
		blockchain.register_node(node)		
	
	response = {
		'message': 'New nodes have been added',
		'total_nodes': list(blockchain.nodes),
	}
	return jsonify(response), 201

# Update the blockchain of the node to the longest existing chain
@app.route('/nodes/resolve', methods=['GET'])
def consensus():
	replaced = blockchain.resolve_conflicts()	
	
	if replaced:
		response = {
			'message': 'Our chain was replaced',
			'new_chain': blockchain.chain
		}
	else:
		response = {
			'message': 'Our chain is authoritative',
			'chain': blockchain.chain
		}
	
	return jsonify(response), 200

# Send a new block, to add to the local chain
@app.route('/block/new', methods=['POST'])
def recieve_block():
	global block_recieved_count
	block_recieved_count = block_recieved_count + 1
	values = request.get_json(force=True)
	# Check that the required fields are in the POST'ed data
	required = ['block'] 
	if not all(k in values for k in required):
		return 'Missing values', 400
	# Check if the block is valid
	chain = blockchain.chain.copy()
	chain.append(values['block'])	
	if blockchain.valid_chain(chain):
		# Reset the current list of transactions
		blockchain.current_transactions = []
		blockchain.chain.append(values['block'])
		blockchain.merkletree.append_leaf(values['block'])
		blockchain.merkletree.build_tree()
		# Gathering statistics
		time_current_block = datetime.datetime.now()		
		if len(blockchain.chain)>1:
			log(datetime.datetime.strftime(time_current_block, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Recieved new block with ID "+str(values['block']['index'])+
				" from "+values['node_identifier']+" ("+str(request.remote_addr)+")")	
		response = {'message': f'Block will be added to the local chain'}			
	else:
		time_current_block = datetime.datetime.now()
		log(datetime.datetime.strftime(time_current_block, "%Y-%m-%d %H:%M:%S")+" "+blockchain.node_identifier+" : Discarded new block with ID "+str(values['block']['index'])+
			" from "+values['node_identifier']+" ("+str(request.remote_addr)+")")	
		response = {'message': f'Not a valid block'}
	return jsonify(response), 201		

# Stop the Node
@app.route('/node/exit', methods=['GET'])
def node_exit():
	thread.stop()
	thread.exit()
	sensor.stop()
	sensor.exit()
	time.sleep(10)
	get_stats()
	time.sleep(2)	
	#sys.exit()
	shutdown_server()
	response = {'message': 'Server shutting down'}
	return jsonify(response), 200
	
###############
# Main method #
###############	

if __name__ == '__main__':
	from argparse import ArgumentParser
	
	parser = ArgumentParser()
	parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
	parser.add_argument('-f', '--file', default="Sensor1.txt", type=str, help='file with sensor data')
	parser.add_argument('-l', '--logging', default="True", type=str, help='Should the programm log events')
	args = parser.parse_args()
	port = args.port
	blockchain.address="127.0.0.1:"+str(port)
	filename = args.file
	
	logging = args.logging == "True"
	starttime = datetime.datetime.now()
	log(datetime.datetime.strftime(starttime, "%Y-%m-%d %H:%M:%S")+" : Node "+blockchain.node_identifier+" started at 127.0.0.1:"+str(port))
	
	app.run(host='127.0.0.1', port=port, threaded=True)