import sys
import subprocess
import requests
import time
import pandas as p
import numpy as n	
	
if __name__ == '__main__':
	from argparse import ArgumentParser
	
	parser = ArgumentParser()
	parser.add_argument('-n', '--nodes', default=1, type=int, help='amount of nodes to run')
	parser.add_argument('-f', '--filename', default="blockchain.py", type=str, help='executable')
	parser.add_argument('-d', '--data', default="data.csv", type=str, help='Sensordata')	
	parser.add_argument('-l', '--logging', default="True", type=str, help='Should the programm log events')
	parser.add_argument('-t', '--time', default=1, type=int, help='number of 10 minute circles')	
	args = parser.parse_args()
	nodes = args.nodes
	logging = args.logging == "True"
	filename = args.filename
	filename_data = args.data
	circles = abs(args.time)	

	# Prepare the sensordata files
	data = p.read_csv(filename_data,
					quotechar='"',
					sep=",",
					decimal=".")
	i=1
	for name in data.sensorId.unique()[0:nodes]:
		sensor_filter = data["sensorId"]==name
		table = data[sensor_filter]
		table_sorted = table.sort_values(by=["createdAt"],ascending=[1])
		f = open("Sensor"+str(i)+".txt","w")
		for value in table_sorted["value"]:
			f.write(str(value)+"\n")
		f.close()
		i=i+1
	
	# Build arguments for subprocesses
	ports = []
	addresses = []
	sensoren = []
	output = []
	for i in range(nodes):
		ports.append(5000+i)
		addresses.append('127.0.0.1:'+str(5000+i))
		sensoren.append('Sensor'+str(i+1)+'.txt')
		output.append('Output'+str(i)+'.txt')
	
	try:
		outfile = open('Logfile.txt','w')
	
		# Start subprocesses
		procs = []
		for i in range(nodes):
			proc = subprocess.Popen([sys.executable, filename, '-p '+str(ports[i]), '-f'+str(sensoren[i]), '-l'+str(logging)], stdout=outfile)		
			procs.append(proc)	
		time.sleep(5)
		proc = subprocess.Popen([sys.executable, "run_test.py", '-n '+str(nodes), '-t '+str(circles)])
		for proc in procs:
			proc.communicate()	
	except KeyboardInterrupt:
		for proc in procs:
			proc.terminate()	
		print("Terminated via KeyboardInterrupt")