import sys
import subprocess
import requests
import time
import datetime
import json
import threading
	
if __name__ == '__main__':
	from argparse import ArgumentParser
	
	parser = ArgumentParser()
	parser.add_argument('-n', '--nodes', default=1, type=int, help='amount of nodes to run')
	parser.add_argument('-t', '--time', default=1, type=int, help='number of 10 minute circles')
	args = parser.parse_args()
	nodes = args.nodes
	circles = abs(args.time)
	
	# Build arguments for subprocesses
	ports = []
	addresses = []
	output = []
	for i in range(nodes):
		ports.append(5000+i)
		addresses.append('127.0.0.1:'+str(5000+i))
		output.append('Output'+str(i)+'.txt')
	
	try:
			
		# Stats pre
		for i in addresses:	
			response = requests.get(f'http://{i}/stats')
			
		# Register the adresses for each node
		for i in addresses:
			n = addresses.index(i)
			temp_addresses = [x for i,x in enumerate(addresses) if i!=n]
			data =  {
				'nodes': temp_addresses
			}
			response = requests.post(f'http://{i}/nodes/register', json=data)

		# Start each node with a capital of 100 coins
		#for i in addresses:
		#	data =  {
		#		'usage': "Coins",
		#		'sender': "Initiation",
		#		'recipient': i,
		#		'amount': 100
		#	}
		#	response = requests.post(f'http://{i}/broadcast/transactions', json=data)	
		#	print(str(i)+": Initiated Capital")				
		# Start mining
		for i in addresses:	
			response = requests.get(f'http://{i}/mine/start')	
		# Start sensor
		for i in addresses:		
			response = requests.get(f'http://{i}/sensor/start')	
		print("Waiting for Blockchain to work ...")
		# Call stats in circles of 10 Minutes
		while(circles>0):	
			time.sleep(600)
			if circles>1:
				for i in addresses:	
					response = requests.get(f'http://{i}/stats')
			circles = circles-1
		print("Calculating final stats ...")

		# Stats post
		for i in addresses:
			threading.Thread(target=requests.get, args=(f'http://{i}/node/exit', )).start()
	except KeyboardInterrupt:
		print("Terminated via KeyboardInterrupt")
		