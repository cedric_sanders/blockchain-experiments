# README #

### What is this repository for? ###

* This Repository contains Python implementations for four Blockchain onsensus algorithms. It also contains scripts to set up a simulated Blockchain network on your localhost 
* and testdata from https://opensensemap.org/
* Version 1.0

### How do I get set up? ###

* To run the code you need a Python enviorment with Python 3.6.6
* I also recommend a package managment system like anaconda(https://www.anaconda.com/).
* You need the following Python packages: Requests, Pandas, Jsonpickle, Psutil, Flask
* To run an experiment make sure all the files are in the same place and start a python terminal. Then simply enter python run_test2.py -n [nodes] -f [consensus file] -t [Time in 10 minute steps] -l [Logging?]
* For example: python run_test2.py -n 5 f blockchain_pow.py -t 6 -l True
* The programm will output a logfile with all the occured events